#ifndef tda_USUARIO_H_INCLUDED
#define tda_USUARIO_H_INCLUDED
#include<iostream>
#include<cstring>
#include<cstdlib>

using namespace std;
////////////////////////////////////////////// DEFINICION DE VARIABLES////////////////////////////////////
struct Usuario{
   string nombre ;
   string apellido;
   string cedula;
   char tipo;
   string email;
   string clave;
   struct Usuario *prox;
   
};

typedef struct Usuario *tListaUsuario;
typedef struct Usuario *tNodoUsuario;

tListaUsuario pListaUsuario;
/////////////////////////////////////////////////////////////////////// FUNCIONES MANEJO DE LISTA //////////////////////////
void insertarFinal(tListaUsuario &pListaUsuario,tNodoUsuario &nodo){
   tListaUsuario p;
   
   if(pListaUsuario==NULL){
      pListaUsuario=nodo ;
   }
   else
   {
      p=pListaUsuario;
      while(p->prox!=NULL){
         p=p->prox;
      }
      p->prox=nodo;
   }

}

void eliminarListaUsuarios(tListaUsuario &pListaUsuario){
   tNodoUsuario nodo;
   while (pListaUsuario->prox!=NULL){
      nodo=pListaUsuario;
      pListaUsuario=pListaUsuario->prox;
      delete(nodo);
   }
   delete(pListaUsuario);
}

void imprimirListaUsuario(tListaUsuario &pListaUsuario){
   tListaUsuario q=pListaUsuario;
   while(q!=NULL){
      cout<<q->nombre<<endl;
      cout<<q->apellido<<endl;
      cout<<q->cedula<<endl;
      cout<<q->tipo<<endl;
      cout<<q->email<<endl;
      cout<<q->clave<<endl;
      cout<<endl;
      q=q->prox;
   }    
}


/////////////////////////////////////////// BLOQUE  LISTA DE USUARIOS A PARTIR DE ARCHIVOS /////////////////////////////////////////



void llenarNodoUsuarioArchivo(tNodoUsuario &nodo,string linea){
  int i=0;
  int j=0;
  int cont=0;
  char c;
  while (i<linea.length())
  {
     if(linea[i]==','){
        cont++;
        switch (cont) {
            case 1:
               nodo->nombre=linea.substr(j,i-j);
               j=i+1;
            break;
            case 2:
               nodo->apellido=linea.substr(j,i-j);
               j=i+1;
           break;
            case 3:
               nodo->cedula=linea.substr(j,i-j);
               
               j=i+1;
            break;
            case 4:
               nodo->tipo=linea[i-1];
               j=i+1;
           break;
            case 5:
               nodo->email=linea.substr(j,i-j);
               j=i+1;
            break;
            case 6:
               nodo->clave=linea.substr(j);
               j=i+1;
            break;
         }      
           
       }
    i++;
   }
  
}

void crearNodoUsuarioArchivo(string linea){
   tNodoUsuario nodo = new(struct Usuario);
   llenarNodoUsuarioArchivo(nodo,linea);
   nodo->prox=NULL; 
   insertarFinal(pListaUsuario,nodo);
}

/////////////////////////////////////////////////////////

#endif