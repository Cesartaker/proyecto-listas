PROYECTO 1.
EXAMENES VIRTUALES. Versión 01
Semestre OCTUBRE-FEBRERO-2020-2021

El objetivo de este proyecto es simular una aplicación que permite por parte del docente administrar exámenes virtuales y por parte de los estudiantes responder las evaluaciones.

Tipos de usuario en la aplicación:
1.	Docente (solo podrá existir un docente en la aplicación)
2.	Estudiantes: Pueden ser uno o más estudiantes

Por defecto tanto el docente como los estudiantes ya están registrados en un archivo de texto, los datos que se deben guardar en el archivo son los siguientes:
•	Nombres.
•	Apellidos.
•	Cédula de identidad.
•	Tipo: D=docente; E=estudiante.
•	Email: debe ser un correo valido.
•	Contraseña (mínimo 8 caracteres).

Cada estudiante puede responder uno o más exámenes, por lo cual cuando un estudiante responda un examen se debe almacenar los datos siguientes:
•	Código de examen: es un valor entero que representa de manera única el examen.
•	Cédula del estudiante: es la cédula del estudiante que respondió el examen.
•	Fecha del examen: fecha en la cual el estudiante hizo el examen. Lo registra la aplicación. El formato es: dd/mm/yyyy.
•	Hora de inicio del examen. La coloca el sistema.
•	Hora de fin del examen. La coloca el sistema.
•	Nota obtenida del 01 al 20.
•	Tiempo de duración respondiendo el examen. (que se puede calcular de los campos hora de inicio y hora de fin del examen.
Aquí es necesario acotar que las respuestas de cada pregunta se deben almacenar también, para poder realizar revisiones a futuro.

Cada examen tiene la siguiente información:
•	Código de examen. Identificador entero único para el examen. Generado por la aplicación
•	Título del examen.
•	Cantidad de preguntas.
•	Puntaje de cada pregunta.
•	Y de alguna manera debe tener la información de las preguntas que están en la prueba.

Las preguntas se almacenan en el banco de preguntas. Para cada Pregunta  se debe almacenar la siguiente información:
•	Código de pregunta: Identificador entero único para la pregunta. Generado por la aplicación
•	Enunciado de la pregunta.
•	Tipo de pregunta: solo hay dos tipos de preguntas:
o	SM: Selección multiple
o	VF: Verdadero o falso.
•	Si el tipo de pregunta es SM se deben incluir todas las opciones que el docente requiera minimo 2 y debe guardarse tambien las opciones que son correctas.
•	Si el tipo de pregunta es VF se deben indicar si el enunciado es verdadero o falso.
Ejemplo1:
Código de pregunta: 10
Enunciado: una variable apuntador de entero almacena un valor tipo entero.
Tipo de pregunta: VF
Respuesta: F
Ejemplo2:
Código de pregunta: 20
Enunciado: Una variable tipo apuntador:
A.	Se declara con un tipo base.
B.	Cuando se declara tiene una dirección valida.
C.	Reserva un espacio de memoria para el tamaño del tipo base y la dirección la guarda en la variable.
D.	Ninguna de las anteriores.
Se debe guardar las opciones que son correctas para poder comparar con las respuestas del estudiante. Las preguntas de selección multiple tienen factor de corrección,  por ejemplo, si una pregunta tiene un puntaje de 1 punto y está compuesta por 4 opciones de las cuales 3 son correctas, si el estudiante respondio una opción incorrecta y dos correctas entonces la nota de la pregunta es 0.333.

Un examen se puede responder en cualquier momento pero solo una vez.

El proyecto debe implementar los siguientes requerimientos:
1.	Autenticación de usuario: permite al usuario entrar a la zona privada: para poder ingresar debe colocar cedula y contraseña. Estos datos deben validarse.


Si el ususuario es el docente:
2.	Agregar pregunta.
3.	Modificar Pregunta. Permite modificar cualquier información. 
4.	Eliminar pregunta. Una pregunta no se puede eliminar si hay un examen registrados que la contiene.
5.	Crear examen. Donde se debe indicar los datos básicos: título, cantidad de preguntas, puntaje por pregunta. Y ademas permitirle al docente seleccionar las preguntas que tendrá el examen.
6.	Modificar examen: se pueden modificar los datos básicos como también cambiar las preguntas que contiene. Siempre y cuando el examen aún no haya sido respondido por al menos un estudiante.
7.	Eliminar examen: elimina el examen pero no las preguntas (ya que estas estan en el banco de preguntas). Un examen no se puede eliminar si al menos un estudiante ya lo respondió.
8.	Mostrar  promedio de notas de todos los estudiantes para todos los examenes incluyendo promedio de tiempo de duración para responder el examen.
9.	Mostrar cuales estudiantes aprobaron un examen dado el código y cuales no lo aprobaron indicando su nota.
10.	Mostrar las notas (incluyendo otros datos como el nombre del examen, tiempo para responder la prueba) de todos los examenes realizados por un estudiante dada la cédula.

Cualquier información que es agregada, modificada o eliminada, despues de realizar la acción debe mostrarse todos los datos. Ejemplo si se modifico una pregunta previamente registrada, una vez que es procesada la petición se debe mostrar al usuario todos los datos de la pregunta modificada


Si el usuario es estudiante:
11.	Responder un examen previamente creado, el cual puede seleccionar de una lista de examenes. Cuando el estudiante responde un examen y finaliza puede ver la nota total. Dejamos a discreción del equipo de programación como indicar las respuestas correctas a cada pregunta si lo hace cada vez que responde una pregunta o al final de responder todo el examen, indicando también la nota obtenida.
12.	Ver nota de un examen en particular. Selecciona el examen de una lista en pantalla que muestra el código del examen, nombre y fecha.
13.	Ver todas las notas.
14.	Ver Promedio de notas de los examenes realizados.
15.	Ver Examen realizado y sus respuestas indicando las respuestas correctas.

IMPORTANTE:  
Toda la información debe guardarse en archivos de texto, cada vez que el usuario: registra, asigna, modifica o elimina automáticamente información esta debe actualizarse en archivos de texto.
Cada vez que se ejecute el programa se carga toda la información que está en el o los archivos de texto en las respectivas estructuras de datos.

Grupos de proyecto:
•	El grupo de proyecto debe ser realizado por 3 personas máximo, los mismos deben estar presentes para la defensa del proyecto y se corrige individualmente.

Características técnicas.
•	El programa debe ser implementado en C++
•	Se debe implementar TDA donde sea necesario (cada TDA debe ser implementado con sus operaciones en librerías), no usar arreglos sino listas dinámicas para almacenar la información. Por eso es importante que cada TDA para cada estructura sea implementada como librería. Ejemplo, estudiante, preguntas, exámenes, y todo aquella estructura que sea necesaria para el proyecto.
•	Todas las lecturas de los archivos deben almacenarse en listas dinámicas primero antes de utilizarse, cualquier nuevo elemento, modificación o eliminación debe realizarse sobre la lista y después guardar los datos en el archivos de texto.
•	Como buena práctica de desarrollo, no se debe repetir código.
•	Se debe modularizar creando librerías (.h) que se utilicen desde el código.
•	Cada función debe hacer una única tarea si realiza  más de una no será evaluada.
•	Cualquier información que el usuario actualice debe haber un resultado que muestre toda la información relacionada con el objeto actualizado.
•	Usar solamente las librerías que son estándares de C++, no se permite otro tipo de librería.
•	El código del proyecto debe estar documentado (estructuras, funciones, librerías).

Fecha Entrega (fecha de entrega: del 14 de Diciembre al 18 de Diciembre) 20%
•	Documentos a entregar:
o	Código fuente del proyecto.
o	Librerías propias necesarias para ejecutar el software.
o	Guía de instalación y uso del software (Si es necesario).

Recomendaciones generales:
•	Definir un jefe de proyecto en el grupo que supervise las actividades (por supuesto, este integrante debe desarrollar también).
•	Definir las actividades que va a realizar cada integrante, analicen primero, definan tareas a realizar,  cuales tareas son prioritarias, estructuras y TDA.
•	Pensar en desarrollos que sean lo más modular posible, no repetir código: principio DRY (don’t repeat yourself) y KISS (“Keep it Simple, Stupid").


