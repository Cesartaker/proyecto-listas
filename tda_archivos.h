#ifndef TDA_ARCHIVO_H_INCLUDED
#define TDA_ARCHIVO_H_INCLUDED
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<fstream>
#include"usuario.h"
using namespace std;

void lecturaUsuarios(){
    ifstream archivo;
    string linea;

    archivo.open("Usuarios.txt",ios::in);// Abriendo archivo Usuario.txt para lectura
    if (archivo.fail()){
        cout<<"NO SE PUDO ABRIR EL ARCHIVO";
        exit(1);
    }
    else
    {
       while(!archivo.eof()){
           getline(archivo,linea);
           fflush(stdin);
           //cout<<linea<<endl;
           crearNodoUsuarioArchivo(linea); // Funcion que se encuentra en el TDA USUARIO
        }
    }

    archivo.close();// Cerrando archivo Usuario.txt
    
}




#endif